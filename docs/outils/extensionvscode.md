---
sidebar_position: 5
---
# Extensions VS Code

- Dracula Official 

- Remote - SSH

- Docker

- Bracket Pair Colorizer 2

- Material Icon Theme

- Settings Sync

- phpcs fixer

- [Symfony extensions pack](https://marketplace.visualstudio.com/items?itemName=duboiss.sf-pack)
