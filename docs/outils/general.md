---
sidebar_position: 1
---
# Général

### Administration
- Keepass : Gestionnaire de mots de passe

- Putty : Connection ssh

- winSCP : ftp ssh


### Organisation Projet

- [Notion](https://www.notionfacile.fr)

- [Trello](https://trello.com/)


### Forum communautaire

- [Stackoverflow](http://stackoverflow.com)

- [Codepen](http://codepen.io)


### Documentation 

- [Langage programmation](https://zealdocs.org)

- [Alternative en ligne](https://devdocs.io)

- [Roadmap Symfony](https://yoanbernabeu.github.io/Roadmap-Dev-Symfony)


### Monitoring Sites et Services

- [Uptime Kuma](https://github.com/louislam/uptime-kuma)


### Analyses traffic

- [Google analytics](https://analytics.google.com) **déconseillé RGPD**

- [Matomo](https://fr.matomo.org/)

### Tester site

- [Gtmetrix](https://gtmetrix.com)

https://feedly.com/i/my  gerer les flux RSS
https://www.cacher.io/ librairie personelle


https://sourcery.ai/ pour python


lucidichart  modele de donnée UML


Génerer du faux texte:
https://www.faux-texte.com/
https://compressor.io/ compresser image pour performance

Pour compresser les images:
https://resizeimage.net

AIRTABLE

Webflow
caard
Typeform
Zapier
Stripe
Secret 
n8n
matomo
swagger
docusaurus
gitlab
pipeline
postman