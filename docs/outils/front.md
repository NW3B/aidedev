---
sidebar_position: 2
---
# Front

### Inspiration

- [Idée de design de site et app](https://dribbble.com)

### Couleurs
- [Couleurs CSS, RGB, Hexa, HSL](https://www.w3schools.com/colors/colors_names.asp)

- [Couleur FLAT](https://flatuicolors.com)

- [Dégradé de couleurs](https://uigradients.com)

- [Dégradé de couleurs 2](https://cssgradient.io)

- [Choix palette de couleur](https://coolors.co)

- [Tester contrast](https://colorable.jxnblk.com)

### Icones

- [Icone fontawesome](http://fontawesome.io)

- [Ionicons](http://ionicons.com)


### Fonts

- [Google fonts](https://fonts.google.com) **exclure cdn pour RGPD**

### Maps

- [Modèle de map](https://snazzymaps.com)

- [Modèle de map 2](https://www.mapbox.com)

- [Modèle de map 3](http://leafletjs.com)

### Images Libre de droit
- [Canva](https://canva.com)

- [Photo unsplash](https://unsplash.com)

- [Photo pexels](https://www.pexels.com/fr-fr)

### Tests et Compatibilité

- [Voir compatibilité entre les navigateurs et les propritées css, fonction javascript, balise html ](https://caniuse.com)

- [Vérifier code HTML](https://validator.w3.org/nu/#textarea)


### Templates gratuit

- [Themewagon](https://themewagon.com)

- [Themefisher](https://themefisher.com/)

- [Boostrapmade](https://bootstrapmade.com)

