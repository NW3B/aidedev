---
sidebar_position: 1
---

# Php extensions

- Installer php
```bash
sudo apt-get install php
```

- Recherchez les extensions spécifiquement pour version PHP 7.4:
```bash
apt-cache search --names-only ^php | grep "php7.4"
```
- simpleXML requis pour SYMFONY :
```bash
sudo apt-get install php-xml
```

- extension zip :
```bash
sudo apt-get install php-zip
```

- extension curl :
```bash
sudo apt-get install php-curl
```
 * mb_strlen() -> mbstring extension.
```bash
sudo apt-get install php7.4-mbstring
```
 * intl extension -> pour validators.
```bash
sudo apt-get install php7.4-intl
```
 * Install PDO drivers -> pour Doctrine.
```bash
sudo apt-get install php7.4-mysql
```
