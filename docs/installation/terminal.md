---
sidebar_position: 6
---
# Terminal

- Afficher le nom de la branche Git sur laquelle on travaille
```bash
nano ~/.bashrc
```
- Copier cela à la fin du fichier:
```jsx title=".bashrc"
# Configuration Git Branch – Modification prompt Linux
parse_git_branch() {
     git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/ (\1)/'
}
export PS1="\u@\h \[\033[32m\]\w\[\033[33m\]\$(parse_git_branch)\[\033[00m\] $ "
```