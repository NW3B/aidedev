---
sidebar_position: 2
---
# Git

- Installer Git:
```bash
apt-get install git
```
[Documentation officielle](https://git-scm.com/book/fr/v2/D%C3%A9marrage-rapide-Installation-de-Git)