---
sidebar_position: 8
---
# Nodejs

- nodeJs
```bash
sudo apt install nodejs
```
- npm
```bash
sudo apt install npm --force
```
