---
sidebar_position: 4
---
# Symfony Cli
[Documentation officielle](https://symfony.com/download)

- Installer Symfony cli :
```bash
echo 'deb [trusted=yes] https://repo.symfony.com/apt/ /' | sudo tee /etc/apt/sources.list.d/symfony-cli.list
```
```bash
sudo apt update
```
```bash
sudo apt install symfony-cli
```

- Tester les extensions pour symfony
```bash
symfony check:requirements
```
